package pl.mateusz_szczukowski.domain;

/**
 * Created by Mateusz on 11.11.2016.
 */
public class Order {

    private long id;
    public ClientDetails client;
    public Address deliveryAddress;
    public OrderItem items;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClientDetails getClient() {
        return client;
    }

    public void setClient(ClientDetails client) {
        this.client = client;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public OrderItem getItems() {
        return items;
    }

    public void setItems(OrderItem items) {
        this.items = items;
    }
}
