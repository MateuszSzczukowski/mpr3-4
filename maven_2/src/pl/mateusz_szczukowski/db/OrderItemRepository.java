package pl.mateusz_szczukowski.db;

import java.util.List;
import pl.mateusz_szczukowski.domain.OrderItem;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface OrderItemRepository extends Repository<OrderItem> {

    public List<OrderItem> name(String Name, OrderItem page);
    public List<OrderItem> description(String Description, OrderItem page);
    public List<OrderItem> price(String Price, OrderItem page);

}
