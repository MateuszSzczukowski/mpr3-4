package pl.mateusz_szczukowski.db;

import pl.mateusz_szczukowski.domain.Order;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface IOrderManager extends Repository<Order> {

}
