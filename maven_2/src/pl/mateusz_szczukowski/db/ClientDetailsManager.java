package pl.mateusz_szczukowski.db;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import pl.mateusz_szczukowski.domain.ClientDetails;

/**
 * Created by Mateusz on 11.11.2016.
 */
public class ClientDetailsManager {

    private Connection connection;

    private String url = "jdbc:hsqldb:hsql://localhost/workdb";

    private String createTableClientDetails = "CREATE TABLE ClientDetails(id bigint GENERATED BY DEFAULT AS IDENTITY,  name varchar(20), surname varchar(20), login varchar(20))";

    private PreparedStatement addClientDetails;
    private PreparedStatement deleteClientDetails;
    private PreparedStatement getClientDetails;
    private PreparedStatement getById;

    private Statement statement;

    public ClientDetailsManager() {
        try {
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();

            ResultSet rs = connection.getMetaData().getTables(null, null, null,
                    null);
            boolean tableExists = false;
            while (rs.next()) {
                if ("ClientDetails".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
                    tableExists = true;
                    break;
                }
            }

            if (!tableExists)
                statement.executeUpdate(createTableClientDetails);

            addClientDetails = connection
                    .prepareStatement("INSERT INTO ClientDetails (id, name, surname, login) VALUES (?, ?, ?, ?)");
            deleteClientDetails = connection
                    .prepareStatement("DELETE FROM ClientDetails");
            getClientDetails = connection
                    .prepareStatement("SELECT id, name, surname, login FROM ClientDetails");
            getById = connection
                    .prepareStatement("SELECT * from address WHERE id = ?");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    Connection getConnection() {
        return connection;
    }

    void clearClientDetails() {
        try {
            deleteClientDetails.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int addClientDetails(ClientDetails details) {
        int count = 0;
        try {
            addClientDetails.setLong(1, details.getId());
            addClientDetails.setString(2, details.getName());
            addClientDetails.setString(3, details.getSurname());
            addClientDetails.setString(4, details.getLogin());

            count = addClientDetails.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public List<ClientDetails> getAddAddress() {
        List<ClientDetails> ClientDetails = new ArrayList<ClientDetails>();

        try {
            ResultSet rs = getClientDetails.executeQuery();

            while (rs.next()) {
                ClientDetails a = new ClientDetails();

                a.setId(rs.getLong("id"));
                a.setName(rs.getString("name"));
                a.setSurname(rs.getString("surname"));
                a.setLogin(rs.getString("login"));

                ClientDetails.add(a);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ClientDetails;
    }

    public ClientDetails getById(int ID) {

        ClientDetails a = new ClientDetails();

        try {
            getById.setInt(1, ID);
            ResultSet rs = getById.executeQuery();

            a.setId(rs.getLong("id"));
            a.setName(rs.getString("name"));
            a.setSurname(rs.getString("surname"));
            a.setLogin(rs.getString("login"));

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }
}