package pl.mateusz_szczukowski.db;

import pl.mateusz_szczukowski.domain.OrderItem;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface IOrderItemManager extends Repository<OrderItem> {
}
