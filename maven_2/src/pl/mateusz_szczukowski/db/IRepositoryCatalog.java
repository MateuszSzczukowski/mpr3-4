package pl.mateusz_szczukowski.db;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface IRepositoryCatalog  {

    public IAddressManager addressManager = new AddressManager();
    public IClientDetailsManager clientDetailsManager = new ClientDetailsManager();
    public IOrderItemManager orderItemManager = new OrderItemManager();

}
