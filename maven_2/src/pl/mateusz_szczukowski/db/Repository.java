package pl.mateusz_szczukowski.db;

import pl.mateusz_szczukowski.domain.OrderItem;

import java.util.List;
import java.sql.Connection;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface Repository<L> {

        public L withId(int id);
        public List<L> getAllOrder(OrderItem page);
        public void addOrder(L entity);
        public void deleteOrder(L entity);
        public void getById(int ID);
}
