package pl.mateusz_szczukowski.db;

import pl.mateusz_szczukowski.domain.ClientDetails;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface IClientDetailsManager extends Repository<ClientDetails> {
}
