package pl.mateusz_szczukowski.db;

import pl.mateusz_szczukowski.domain.Address;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface IAddressManager extends Repository<Address> {
}
