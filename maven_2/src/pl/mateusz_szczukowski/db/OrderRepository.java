package pl.mateusz_szczukowski.db;

import pl.mateusz_szczukowski.domain.Order;
import java.util.List;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface OrderRepository extends Repository<Order> {

    public List<Order> client(String Client, Order page);
    public List<Order> address(String Address, Order page);
    public List<Order> item(String Item, Order page);
}
