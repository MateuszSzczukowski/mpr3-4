package pl.mateusz_szczukowski.db;

import pl.mateusz_szczukowski.domain.ClientDetails;

import java.util.List;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface ClientDetailsRepository extends Repository<ClientDetails> {

    public List<ClientDetails> name(String Name, ClientDetails page);
    public List<ClientDetails> surname(String Surname, ClientDetails page);
    public List<ClientDetails> login(String Login, ClientDetails page);
}
