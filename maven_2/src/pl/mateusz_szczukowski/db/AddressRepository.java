package pl.mateusz_szczukowski.db;

import java.util.List;
import pl.mateusz_szczukowski.domain.Address;

/**
 * Created by Mateusz on 24.11.2016.
 */
public interface AddressRepository extends Repository<Address> {
    public List<Address> street (String Street , Address page);
    public List<Address> buildingNumber(String BuildingNumber, Address page);
    public List<Address> flatNumber(String FlatNumber, Address page);
    public List<Address> postalCode(String PostalCode, Address page);
    public List<Address> city(String City, Address page);
    public List<Address> country(String Country, Address page);
}
